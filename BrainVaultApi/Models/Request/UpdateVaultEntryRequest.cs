﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace ElevaultApi.Models.Request
{
    public class UpdateVaultEntryRequest
    {
        public string[] Events { get; set; }
        public string[] Comments { get; set; }
        public string Mood { get; set; }
        public int DayRating { get; set; }
    }
}
