﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ElevaultApi.Models.Request
{
    public class CreateNotificationRequest
    {
        [MinLength(1)]
        public string UserId { get; set; }
        
        [MinLength(1)]
        public string PushNotificationToken { get; set; }

        [Range(0, 23)]
        public int NotificationHour { get; set; }

        [Range(0,59)]
        public int NotificationMinute { get; set; }
    }
}
