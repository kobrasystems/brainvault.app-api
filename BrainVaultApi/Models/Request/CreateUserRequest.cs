﻿using System.ComponentModel.DataAnnotations;

namespace ElevaultApi.Controllers
{
    public class CreateUserRequest
    {
        [EmailAddress]
        public string EmailAddress { get; set; }
        
        [Required]
        [MinLength(8)]
        public string Password { get; set; }
    }
}