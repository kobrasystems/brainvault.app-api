﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace ElevaultApi.Models.Request
{
    public class UpdateUserRequest
    {
        [JsonPropertyName("_id")]
        public string Id { get; set; }
                
        [EmailAddress]
        public string EmailAddress { get; set; }

        public string Password { get; set; }

        [Range(0,23)]
        public int NotificationHour { get; set; }

        [Range(0,59)]
        public int NotificationMinute { get; set; }

        [MinLength(1)]
        public string PushNotificationToken { get; set; }
    }
}
