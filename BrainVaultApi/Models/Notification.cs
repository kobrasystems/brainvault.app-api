﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ElevaultApi.Models
{
    public class Notification
    {
        [BsonElement("hour")]
        public int Hour { get; set; }
        [BsonElement("minute")]
        public int Minute { get; set; }
        [BsonElement("pushToken")]
        public string PushToken { get; set; }

    }
}
