﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ElevaultApi.Models
{
    public class ElevaultDatabaseSettings : IElevaultDatabaseSettings
    {
        public string UsersCollectionName { get; set; }
        public string VaultEntriesCollectionName { get; set; }
        public string ConnectionString { get; set; }
        public string DatabaseName { get; set; }
    }

    public interface IElevaultDatabaseSettings
    {
        string UsersCollectionName { get; set; }
        string VaultEntriesCollectionName { get; set; }
        string ConnectionString { get; set; }
        string DatabaseName { get; set; }
    }
}
