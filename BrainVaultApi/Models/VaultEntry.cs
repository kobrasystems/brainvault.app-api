﻿using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace ElevaultApi.Models
{
    [BsonIgnoreExtraElements]
    public class VaultEntry
    {
        [BsonId]
        [BsonRepresentation(MongoDB.Bson.BsonType.ObjectId)]
        [JsonPropertyName("_id")]
        public string Id { get; set; }
        
        [JsonPropertyName("userId")]
        public string UserId { get; set; }

        [BsonElement("events")]
        public string[] Events { get; set; }
        
        [BsonElement("comments")]
        public string[] Comments { get; set; }
        
        [BsonElement("mood")]
        public string Mood { get; set; }
        
        [BsonElement("dayRating")]
        public int DayRating { get; set; }
        
        [BsonElement("date")]
        public DateTime Date { get; set; }
    }
}
