﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace ElevaultApi.Models
{
    public class User
    {
        [BsonId]
        [BsonRepresentation(MongoDB.Bson.BsonType.ObjectId)]
        [JsonPropertyName("_id")]
        public string Id { get; set; }

        [JsonPropertyName("emailAddress")]
        public string EmailAddress { get; set; }

        public string PasswordHash { get; set; }

        [JsonPropertyName("notification")]
        public Notification Notification { get; set; }
    }
}
