﻿using ElevaultApi.Models;
using ElevaultApi.Models.Request;
using ElevaultApi.Services;
using ElevaultApi.Services.Database;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ElevaultApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VaultEntryController : ControllerBase
    {
        private readonly VaultEntryService _vaultEntryService;
        private readonly AuthService _authService;

        public VaultEntryController(VaultEntryService vaultEntryService, AuthService authService)
        {
            _vaultEntryService = vaultEntryService;
            _authService = authService;
        }

        [HttpGet]
        [Authorize]
        public IActionResult Get(string startDate, string endDate)
        {
            var accountId = _authService.GetAccountIdFromRequest(Request);

            if (string.IsNullOrEmpty(accountId))
            {
                return Unauthorized();
            }

            if (DateTime.TryParse(startDate, out DateTime parsedStart) && DateTime.TryParse(endDate, out DateTime parsedEnd))
            {
                var vaultEntries = _vaultEntryService.Get().Where(v => v.UserId == accountId && v.Date >= parsedStart && v.Date <= parsedEnd);
                return Ok(vaultEntries);
            }
            else
            {
                return BadRequest();
            }

        }

        [HttpPost]
        [Authorize]
        public IActionResult Post([FromBody] CreateVaultEntryRequest vaultEntry)
        {
            var accountId = _authService.GetAccountIdFromRequest(Request);

            if (string.IsNullOrEmpty(accountId))
            {
                return Unauthorized();
            }

            VaultEntry dbVaultEntry = new VaultEntry
            {
                UserId = accountId,
                Mood = vaultEntry.Mood,
                Comments = vaultEntry.Comments,
                Date = vaultEntry.Date,
                DayRating = vaultEntry.DayRating,
                Events = vaultEntry.Events
            };

            var newVaultEntry = _vaultEntryService.Create(dbVaultEntry);

            return Ok(newVaultEntry);
        }

        [HttpPut("{vaultEntryId}")]
        [Authorize]
        public IActionResult Put(string vaultEntryId, [FromBody] UpdateVaultEntryRequest vaultEntry)
        {
            var accountId = _authService.GetAccountIdFromRequest(Request);

            if (string.IsNullOrEmpty(accountId))
            {
                return Unauthorized();
            }

            var vaultEntryToUpdate = _vaultEntryService.Get().SingleOrDefault(v => v.UserId == accountId && v.Id == vaultEntryId);

            if (vaultEntryToUpdate == null)
            {
                return NotFound();
            }

            var updatedVaultEntry = new VaultEntry
            {
                Id = vaultEntryId,
                Comments = vaultEntry.Comments,
                Events = vaultEntry.Events,
                DayRating = vaultEntry.DayRating,
                Mood = vaultEntry.Mood,
                Date = vaultEntryToUpdate.Date,
                UserId = accountId
            };

            _vaultEntryService.Update(vaultEntryId, updatedVaultEntry);

            return Ok(updatedVaultEntry);
        }
    }
}
