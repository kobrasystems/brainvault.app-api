﻿using ElevaultApi.Helpers;
using ElevaultApi.Models.Request;
using ElevaultApi.Services;
using ElevaultApi.Services.Database;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ElevaultApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly UserService _userService;
        private readonly AuthService _authService;

        public UserController(UserService userService, AuthService authService)
        {
            _userService = userService;
            _authService = authService;
        }

        //[HttpPost]
        //public IActionResult Create([FromBody]CreateUserRequest model)
        //{
        //    // do JWT stuff
            
        //    _userService.Create(new Models.User
        //    {
        //        EmailAddress = model.EmailAddress,
        //        PasswordHash = Hashing.GetBCryptHash(model.Password),
        //    });


        //}

        [HttpPut]
        public IActionResult Put([FromBody]UpdateUserRequest model)
        {
            var userId = _authService.GetAccountIdFromRequest(Request);

            if (string.IsNullOrEmpty(userId))
            {
                return Unauthorized();
            }

            var user = _userService.Get(userId);

            string passwordHash = user.PasswordHash;

            if(!string.IsNullOrWhiteSpace(model.Password))
                passwordHash = Hashing.GetBCryptHash(model.Password);

            _userService.Update(userId, new Models.User
            {
                EmailAddress = model.EmailAddress,
                PasswordHash = passwordHash,
                Notification = new Models.Notification
                {
                    Hour = model.NotificationHour,
                    Minute = model.NotificationMinute,
                    PushToken = model.PushNotificationToken
                },
            });

            return Ok();
        }
    }
}
