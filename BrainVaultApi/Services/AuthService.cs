﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Primitives;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElevaultApi.Services
{
    public class AuthService
    {
        public static TokenValidationParameters GetTokenValidationParameters(string jwtKey)
        {
            var jwtKeyBytes = Encoding.UTF8.GetBytes(jwtKey);

            return new TokenValidationParameters
            {
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(jwtKeyBytes),
                ValidateIssuer = false,
                ValidateAudience = false,
                ClockSkew = TimeSpan.Zero

            };
        }

        public string GetAccountIdFromRequest(HttpRequest request)
        {
            if(request.Headers.TryGetValue("Authorization", out StringValues authHeaders))
            {
                string authHeader = authHeaders.ToString();
                if(string.IsNullOrEmpty(authHeader))
                {
                    return null;
                }
                else
                {
                    string authTokenStr = authHeader.Substring(7);
                    JwtSecurityTokenHandler handler = new JwtSecurityTokenHandler();
                    JwtSecurityToken authToken = handler.ReadJwtToken(authTokenStr);

                    var subClaim = authToken.Claims.SingleOrDefault(c => c.Type == "sub");

                    if(subClaim == null)
                    {
                        return null;
                    }

                    return subClaim.Value;
                }
            }
            else
            {
                return null;
            }
        }
    }
}
