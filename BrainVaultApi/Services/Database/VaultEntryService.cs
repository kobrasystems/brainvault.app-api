﻿using ElevaultApi.Models;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ElevaultApi.Services.Database
{
    public class VaultEntryService
    {
        private readonly IMongoCollection<VaultEntry> _vaultEntries;

        public VaultEntryService(IElevaultDatabaseSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);

            _vaultEntries = database.GetCollection<VaultEntry>(settings.VaultEntriesCollectionName);
        }

        public List<VaultEntry> Get() => _vaultEntries.Find(vaultEntry => true).ToList();

        public VaultEntry Get(string id) => _vaultEntries.Find<VaultEntry>(vaultEntry => vaultEntry.Id == id).FirstOrDefault();

        public VaultEntry Create(VaultEntry vaultEntry)
        {
            _vaultEntries.InsertOne(vaultEntry);
            return vaultEntry;
        }

        public void Update(string id, VaultEntry vaultEntryIn) => _vaultEntries.ReplaceOne(vaultEntry => vaultEntry.Id == id, vaultEntryIn);

        public void Remove(VaultEntry vaultEntry) => _vaultEntries.DeleteOne(vaultEntry => vaultEntry.Id == vaultEntry.Id);

        public void Remove(string id) => _vaultEntries.DeleteOne(vaultEntry => vaultEntry.Id == id);
    }
}
